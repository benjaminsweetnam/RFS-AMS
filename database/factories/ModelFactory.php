<?php
$faker = Faker\Factory::create('en_AU');
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(RFS_Manager\Member::class, function (Faker\Generator $faker) {
  return [
    'first' => $faker->firstName,
    'last' => $faker->lastName,
    'mobile' => $faker->phoneNumber,
  ];
});

$factory->define(Event::class, function (Faker\Generator $faker) {
  return [
    'message' => $faker->realText($faker->numberBetween(10, 160)),
  ];
});

$factory->define(RFS_Manager\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(RFS_Manager\Qual::class, function (Faker\Generator $faker) {
  return [
    'qualification' => $faker->name,
    'member_id' => str_random(2),
  ];
});

$factory->define(RFS_Manager\AccessRoles::class, function (Faker\Generator $faker) {
  return [
    'member_id' => str_random(2),
    'username' => $faker->name,
    'password' => $faker->password,
  ];
});

// $factory->define(RFS_Manager\Member::class, function(Faker\Generator $faker) {
//   return [
//     'member_id' => str_random(2),
//     'event_id' => $faker->numberBetween(0, 100),
//     'response' => $faker->realText($faker->numberBetween(10, 160)),
//   ];
// });


