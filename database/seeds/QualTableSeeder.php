<?php

use Illuminate\Database\Seeder;

class QualTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qualifications')->insert(
        	['shortcode' => 'BF', 'qualification' => 'Bush Fire Fighter', 'created_at' => date("Y-m-d h:i:sa"), 'updated_at' => date("Y-m-d h:i:sa")],
        	['shortcode' => 'AF', 'qualification' => 'Advanced Firefighter', 'created_at' => date("Y-m-d h:i:sa"), 'updated_at' => date("Y-m-d")],
        	['shortcode' => 'CL', 'qualification' => 'Crew Leader', 'created_at' => date("Y-m-d h:i:sa"), 'updated_at' => date("Y-m-d h:i:sa")],
        	['shortcode' => 'VF', 'qualification' => 'Village Firefighter', 'created_at' => date("Y-m-d h:i:sa"), 'updated_at' => date("Y-m-d h:i:sa")],
        	['shortcode' => 'DV', 'qualification' => 'Driver', 'created_at' => date("Y-m-d h:i:sa"), 'updated_at' => date("Y-m-d h:i:sa")]);
    }
}
