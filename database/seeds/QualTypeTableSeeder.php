<?php

use Illuminate\Database\Seeder;

class QualTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qualification_type')->insert(
        	['qualification_id' => '1', 'type_id' => '1']);
    }
}
