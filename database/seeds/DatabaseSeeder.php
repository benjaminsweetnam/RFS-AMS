<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(QualTableSeeder::class);
        $this->command->info('Default Qualifications Added');
        $this->call(QualTypeTableSeeder::class);
        $this->command->info('Default Fire Call Types Added');

        Model::reguard();
    }
}
