<?php

namespace RFS_Manager;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = array('message');


   public function type()
   {
   		return $this->belongsToMany(Type::class);
   }

   public function responses()
   {
   		return $this->hasMany(Response::class);
   }

}