<?php

namespace RFS_Manager\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Support\Facades\Log;
use RFS_Manager\Events\EventCreated;
use RFS_Manager\Jobs\sendAlert;
use RFS_Manager\Member;

class SmsEvent
{

    public $members;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->members = Member::all();
    }

    /**
     * Handle the event.
     *
     * @param  EventCreated  $event
     * @return void
     */
    public function handle(EventCreated $event)
    {
        $message = $event->event;
        foreach($this->members as $member)
        {
            dispatch(new sendAlert($member, $message));
            Log::info('Message: '.$message.' sent to '.$member);
        }
    }
}
