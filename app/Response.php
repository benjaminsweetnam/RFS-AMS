<?php

namespace RFS_Manager;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'responses';

    protected $fillable = ['message'];

    public function member()
    {
    	return $this->belongsTo(Member::class);
    }

    public function event()
    {
    	return $this->belongsTo(Event::class);
    }
}
