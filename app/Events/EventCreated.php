<?php

namespace RFS_Manager\Events;

use RFS_Manager\Events\Event;
use RFS_Manager\Event as Alert;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventCreated extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Alert $event)
    {
        $this->event = $event->message;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['event-channel'];
    }
}
