<?php

namespace RFS_Manager\Events;

use RFS_Manager\Events\Event;
use RFS_Manager\Response;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ResponseRecieved extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $body;
    public $member;
    public $event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Response $response)
    {
        $this->body = $response->message;
        $this->member = $response->member_id;
        $this->event = $response->event_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['event-channel'];
    }
}
