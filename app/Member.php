<?php

namespace RFS_Manager;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
	protected $table = 'members';

	protected $fillable = array('first', 'last', 'mobile', 'qualification');

	public function qualification()
    {
    	return $this->belongsToMany(Qualification::class);
    }

    public function getQualificationsPaginated()
    {
    	return $this->qualification()->paginate(10);
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }
}
