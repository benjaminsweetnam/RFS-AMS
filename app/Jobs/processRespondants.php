<?php

namespace RFS_Manager\Jobs;

use Carbon\Carbon;
use RFS_Manager\Jobs\Job;
use RFS_Manager\Member;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class processRespondants extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $sent;
    protected $member;
    protected $direction;
    protected $sid;
    protected $lsid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message, $sid)
    {
        $this->sent = Carbon::parse($message->date_sent);
        $this->member = Member::where('mobile', '=', $message->from)->first();
        $this->direction = $message->direction;
        $this->sid = $message->sid;
        $this->lsid = $sid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            if($this->direction == "inbound" && $this->sid != $this->lsid){
                 if(!empty($member)){
                list($response, $response_time) = explode(' ' , $message->body);
                if(strToLower(trim($response)) == "yes"){
                  $due = Carbon::now()->diffInMinutes($sent->copy()->addMinutes($response_time), False);
                if($due < 0){
                  $member->time = $due." mins";
                } else {
                  $member->time = $due." mins";
                }
                if(in_array($member, $respondants)){
                  continue;
                }else{
                  $respondants[] = $member;
                }
              }
              if($response == "no"){
                $not [] = $member;
              }
            }
          }
    }
}




