<?php

namespace RFS_Manager\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use RFS_Manager\Events\Event;
use RFS_Manager\Jobs\Job;
use RFS_Manager\Jobs\sendSms;
use RFS_Manager\Member;
use Services_Twilio;

class sendAlert extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $request;
    protected $member;
    protected $message;
    protected $twilioNumber;
    protected $sms;
    protected $client;
    protected $auth_token;
    protected $account_sid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, $message)
    {
        $this->member = $member;
        $this->account_sid = env('TWILIO_ACCOUNT_SID');
        $this->auth_token = env('TWILIO_AUTH_TOKEN');
        $this->twilioNumber = env('TWILIO_NUMBER');
        $this->sms = 'RFS Alert - '.$message.' please respond yes and ETA to '.$this->twilioNumber;
        $this->client = new Services_Twilio($this->account_sid, $this->auth_token);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info($this->sms.'to'.$this->member);
        // $this->client->account->messages->create(array(
        //                 'To' => $this->member->mobile,
        //                 'From' => $this->twilioNumber,
        //                 'Body' => $this->sms
        //             ));
    }
}
