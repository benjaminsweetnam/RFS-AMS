<?php

namespace RFS_Manager;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';

    protected $fillable = ['callType'];

    public function qualifications()
    {
    	return $this->belongsToMany(Qualification::class);
    }

    public function events()
    {
    	return $this->belongsToMany(Event::class);
    }
}
