<?php

namespace RFS_Manager;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
	protected $table = 'qualifications';

	protected $fillable = array('shortcode', 'qualification');

	public function members()
	{
		return $this->belongsToMany(Member::class);
	}

	public function types()
	{
		return $this->belongsToMany(Type::class);
	}
}
