<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use RFS_Manager\Event;
use RFS_Manager\Events\EventCreated;
use RFS_Manager\Events\EventRecieved;
use RFS_Manager\Events\NewEventRecieved;
use RFS_Manager\Events\UserHasRegistered;
use RFS_Manager\Http\Requests\EventRequest;
use RFS_Manager\Http\Requests\Request;
use RFS_Manager\Member;
use RFS_Manager\User;
use RFS_Manager\jobs\SendAlert;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);
Route::get('/calls', ['as' => 'events', 'uses' => 'EventController@index']);
Route::get('/calls/new', 'EventController@create');
Route::post('/calls', ['as' => 'event_store', 'uses' => 'EventController@store']);
Route::get('/test', ['as' => 'test', 'uses' => 'EventController@test']);
Route::get('/calls/response', ['as' => 'sms.response', 'uses' => 'EventController@response']);
Route::get('/calls/send', ['as'=>'repeater', 'uses'=>'EventController@repeater']);
Route::get('/init', ['as'=>'init', 'uses'=>'MemberController@init']);
Route::get('/assign/{id}', ['as'=>'qualifications.assign', 'uses'=>'QualificationController@assign']);
Route::post('/assign/{id}', 'MemberController@store_assignment');
Route::get('/unassign/{mid}/{qid}', ['as'=>'qualifications.unnassign','uses'=>'MemberController@remove_assignment']);
Route::get('/types/assign/{id}', ['as'=>'types.assign', 'uses'=>'TypeController@assign']);
Route::post('/types/assign/{id}', 'TypeController@store_assignment');
Route::post('/api/event', 'EventController@callback');

Route::get('socket', 'EventController@viewEvent');
Route::post('sendmessage', 'EventController@sendEvent');
Route::get('writemessage', 'EventController@writeEvent');
Route::get('twilio_response', 'EventController@twilioResponse');

Route::resource('types', 'TypeController');
Route::resource('users', 'AccessController');
Route::resource('qualifications', 'QualificationController');
Route::resource('members', 'MemberController');

Route::get('/types/{type}/{qshort}', ['as'=>'types.unnassign', 'uses'=>'TypeController@remove_assignment']);


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Route::get('/testmessage', 'DashboardController@response');
Route::get('/testmessage', function() {
	$event = Event::all()->first();
    Event::fire(new EventCreated($event));
});

Route::get('/fullscreen', ['as'=>'full_screen','uses'=>'DashboardController@fullscreen']);

// Route::post('/api/event/test', 'EventController@writeFile');


