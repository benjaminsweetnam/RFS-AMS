<?php

namespace RFS_Manager\Http\Controllers;

use Barryvdh\Debugbar\Middleware\Debugbar;
use Carbon\Carbon;
use Event;
use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use LRedis;
use RFS_Manager\Event as Alert;
use RFS_Manager\Events\EventCreated;
use RFS_Manager\Events\EventRecieved;
use RFS_Manager\Events\ResponseRecieved;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Middleware;
use RFS_Manager\Http\Requests;
use RFS_Manager\Http\Requests\EventRequest;
use RFS_Manager\Jobs\CompileReports;
use RFS_Manager\Jobs\SendAlert;
use RFS_Manager\Member;
use RFS_Manager\Qualification;
use RFS_Manager\Response;
use RFS_Manager\Type;
use Services_Twilio;


class EventController extends Controller
{
    use DispatchesJobs;

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['response', 'repeater', 'callback']]);

        $this->members = Cache::remember('members', 120, function() {
          // return Member::all();
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Alert::with(['type'])->orderBy('created_at', 'desc')->paginate(10);

        return view('event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Alert;
        $qualifications = Qualification::lists('qualification', 'shortcode');
        $event_type = Type::lists('callType', 'id');
        return view('event.create', compact('event', 'qualifications', 'event_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $event_type = Type::find($request->event_type);
        $members = $this->members;
        $alert = new Alert;
        $alert->message = $request->message;
        $alert->save();
        Event::fire(new EventCreated($alert));
        $alert->type()->attach($request->event_type);
        return redirect('/');//->with('status', 'Alert Sent');
    }

    // public function repeater(Request $request)
    // {
    //     // gets all users
    //     $members = $this->members;
    //     $messages = $client->account->messages;
    //     foreach ($messages as $message) {
    //         if ($message->direction == 'inbound' && startsWith(strToLower($message->body), 'alert')) {
    //             $inbound = $message->body;
    //             break;
    //         }
    //     };
    //     list($test, $message) = explode('-', $inbound);
    //     if (strToLower(trim($test)) == "alert") {
    //         $event = new Alert;
    //         $event->message = 'RFS Alert - '.$message.' please respond yes and ETA to '.$twilioNumber;
    //         // foreach ($members as $member) {
    //         //     $this->dispatches(new SendAlert($member, $request));
    //         // }
    //         $event->save();
    //         $response = Response::make($client, 200);
    //         $response->header('Content-Type', 'text/xml');
    //         return $response;
    //     };

    // }

    public function callback(Request $request)
    {
        $input   = $request->all();
        $message = $input['Body'];
        $from    = $input['From'];
        $rest    = "";

        /**
         * sid
         * date_created
         * date_updated
         * date_sent
         * account_sid
         * from
         * to
         * body
         * status
         * direction
         * price
         * api_version
         * uri
         */
        $messageArray = explode(' ', $message);
        for($i = 1; $i<=count($messageArray) - 1; $i++)
        {
            $rest = $rest . ' ' . $messageArray[$i];
        }
        $trigger = strToLower(trim($messageArray[0]));
            if($trigger === 'fire call')
            {
                $response = new Alert;
                $response->message = $rest;
                $response->save();
                event(new EventCreated($response));
            }elseif($trigger == 'no')
            {
                $member = Member::where('mobile', $from)->first();
                $event = Alert::all()->last();
                $response = new Response;
                $response->message = $trigger;
                $response->member_id = $member->id;
                $response->event_id = $event->id;
                $response->save();
                event(new ResponseRecieved($response));
            }else{
                $member = Member::where('mobile', $from)->first();
                $event = Alert::all()->last();
                $response = new Response;
                $response->message = $rest;
                $response->member_id = $member->id;
                $response->event_id = $event->id;
                $response->save();
                event(new ResponseRecieved($response));
            }
            $xml = "<?xml version='1.0' encoding='UTF-8'?>
            <Response>
            </Response>";
            return response($xml, '200')->header('Content-Type', 'application/xml');
    }

    public function SendAlert($event)
    {
        $members = Member::all();
        dd($event);
        foreach($members as $member)
        {
            $this->dispatch(new sendAlert($member, $event));
            Log::info('Message: '.$event.' sent to '.$member);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
