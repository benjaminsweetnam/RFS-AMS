<?php

namespace RFS_Manager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use RFS_Maganer\Event;
use RFS_Manager\Http\Controllers\Auth;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Requests;
use RFS_Manager\Http\Requests\MemberRequest;
use RFS_Manager\Member;
use RFS_Manager\Qualification;
use RFS_Manager\User;

class MemberController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'init']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::with('qualification')->orderBy('first', 'asc')->paginate(7);
        // $qualifications = Qualification::all();

        return view('members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = new Member;

        return view('members.create', compact('member'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $member = new Member;
        $member->first = $request->first;
        $member->last = $request->last;
        if(startsWith(trim($request->mobile), '0')){
            $member->mobile = str_replace('0', '+61', trim($request->mobile));
        }else{
            $member->mobile = $request->mobile;
        }
        $member->save();
        return redirect()->action('QualificationController@assign', [$member->id]);/**->with('status', 'Member Added')**/
    }

    public function store_assignment(Request $request, $id)
    {
        $member = Member::find($id);
        $qualification = Qualification::where('shortcode', $request->qualifications)->get();
        $member->qualification()->attach($qualification[0]->id);
        return Redirect::back();
    }

    public function remove_assignment($mid, $qcode)
    {
        $member = Member::find($mid);
        $qualification = Qualification::where('shortcode', $qcode)->get();
        $member->qualification()->detach($qualification[0]->id);

        return Redirect::back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::findOrFail($id);

        return view('members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);

        return view('members.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $member = Member::findOrFail($id);
        $request->mobile = str_replace('04', '+614', substr(trim($request->mobile), 0, 2)) + substr(trim($request->mobile), 2, strlen($request->mobile));
        $member->update($request->all());

        return Redirect::route('members.index')/**->with('status', 'Member Updated')**/;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Member::destroy($id);

        return Redirect::route('members.index')/*->with('status', 'Member Deleted')*/;
    }

    public function init()
    {
        $users = (array)User::all();
        $users = array_filter($users);
        if(empty($users))
        {
            $user = new User;
            $user->name = 'Temp';
            $user->email = 'Temp';
            $user->password = Hash::make('RFS_Rock-yoUr-wiLd-lifE');
            $user->save();
        }
        return Redirect::route('home');
    }
}
