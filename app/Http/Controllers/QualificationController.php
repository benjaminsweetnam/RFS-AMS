<?php

namespace RFS_Manager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Requests;
use RFS_Manager\Http\Requests\QualificationRequest;
use Illuminate\Pagination\Paginator;
use RFS_Manager\Member;
use RFS_Manager\Qualification;

class QualificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function assign($id)
    {
        $member = Member::findOrFail($id);
        $memberQualifications = $member->qualification()->get();
        $qualifications = Qualification::lists('qualification', 'shortcode');

        return view('qualifications.assign', compact('qualifications', 'member', 'memberQualifications'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications = Qualification::paginate('5');

        return view('qualifications.index', compact('qualifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $qualification = new Qualification;

        return view('qualifications.create', compact('qualification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QualificationRequest $request)
    {
        Qualification::create($request->all());

        return redirect()->action('QualificationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qualifications = Qualification::where($id);

        return view('qualifications.show', compact('qualification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qualification = Qualification::find($id);

        return view('qualifications.edit', compact('qualification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $qualification = Qualification::find($id);

        $qualification->update($request->all());

        return Redirect::route('qualifications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Qualification::destroy($id);

        return Redirect::route('qualifications.index');
    }
}
