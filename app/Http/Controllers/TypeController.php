<?php

namespace RFS_Manager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Requests;
use RFS_Manager\Http\Requests\TypeRequest;
use RFS_Manager\Qualification;
use RFS_Manager\Type;

class TypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function assign($id)
    {
        $type = Type::findOrFail($id);
        $qualifications = Qualification::lists('qualification', 'shortcode');
        return view('types.assign', compact('type', 'qualifications'));
    }
    public function store_assignment(Request $request, $id)
    {
        $type = Type::find($id);
        $qualification = Qualification::where('shortcode', $request->qualifications)->get();
        $type->qualifications()->attach($qualification[0]->id);

        return Redirect::back();
    }
    public function remove_assignment($tid, $qshort)
    {
        $type = Type::find($tid);
        $qualification = Qualification::where('shortcode', $qshort)->get();
        $type->qualifications()->detach($qualification[0]->id);
        return Redirect::back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::with('qualifications', 'events')->get();

        return view('types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new Type;
        return view('types.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeRequest $type)
    {
        Type::create($type->all());

        return Redirect::route('types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Type::find($id);

        return view('types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = Type::find($id);

        $type->update($request->all());

        return Redirect::route('types.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Type::destroy($id);

        return Redirect::route('types.index');
    }
}
