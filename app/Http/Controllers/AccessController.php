<?php

namespace RFS_Manager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use RFS_Manager\Http\Controller\Auth;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Requests;
use RFS_Manager\Http\Requests\UserRequest;
use RFS_Manager\Http\Requests\UserUpdateRequest;
use RFS_Manager\Member;
use RFS_Manager\User;

class AccessController extends Controller
{

    protected $redirectPath = '/';
    protected $loginPath = '/auth/login';
    public function __construct()
    {
        return $this->middleware('auth', ['except'=>['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = User::all();

        return view('accounts.index', compact('accounts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;

        return view('accounts.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::where('name', 'Temp')->get() != '[]')
        {
            $temp = User::where('name', 'Temp')->get();
            User::destroy($temp[0]->id);
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return Redirect::route('users.index')/**->with('status', 'Account Added')**/;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = User::find($id);

        return view('accounts.show', compact('account'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('accounts.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }
        $user->update();

        return Redirect::route('users.index')/*->with('status', 'Account Updated')*/;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::destroy($id);

        return Redirect::route('users.index')->with('status', 'Account Deleted');
    }

    public function authenticate()
    {
        if (Auth::attempt(['username'=>$username, 'password'=>$password])) {
            return redirect()->intended('/');
        }
    }
}
