<?php

namespace RFS_Manager\Http\Controllers;

use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use RFS_Manager\Event;
use RFS_Manager\Http\Controllers\Auth;
use RFS_Manager\Http\Controllers\Controller;
use RFS_Manager\Http\Requests;
use RFS_Manager\Jobs\getMessages;
use RFS_Manager\Jobs\processRespondants;
use RFS_Manager\Member;
use RFS_Manager\User;
use RFS_Manager\Response;
use Services_Twilio;

class DashboardController extends Controller
{
  protected $redirectPath = '/';
  protected $loginPath = '/auth/login';
	public function __construct() {
		$this->middleware('auth', ['except' => ['login']]);
	}
    public function login() {
   		return view('dashboard.index');
    }
    public function index() {
        Carbon::setlocale('en');

        $required = [];
        $still_required = [];
        $abilities = [];
        $respondants = [];
        $last = Event::with(['type', 'responses'])->orderBy('created_at', 'desc')->first();
        $ids = [];
        $i = 0;
        $timeout = Carbon::now()->diffInMinutes(Carbon::parse($last->created_at)->copy()->addMinutes(45));
        // dd($last, $timeout <= 0);
        if($timeout <= 0)
        {
            return view('dashboard.no-event');
        }
        if($last){
            $responses = $last->responses()->get();
            foreach($responses as $res)
            {
                if(!in_array($res->member_id, $ids))
                {
                    $ids[] = $res->member_id;
                    $respondants[] = Member::find($res->member_id);
                }
            }
        }else{
            return view('dashboard.no-event');
        }
        foreach($respondants as $resp)
        {
            if($resp->responses->last()->message != "no"){
             foreach($resp->qualification as $qual)
             {
                $abilities[] = $qual->shortcode;
             }
            }
        }
        foreach($last->type as $type)
        {
            foreach($type->qualifications as $quals)
            {
                $still_required[] = $quals->shortcode;
            }
        }
        foreach($abilities as $ability)
        {
            $still_required = array_values($still_required);
            if(in_array($ability, $still_required)){
                for($x = 0; $x<sizeof($still_required); $x++){
                    if($still_required[$x] == $ability){
                        unset($still_required[$x]);
                    }
                }
            }
        }
      Log::info('finished');
    return view('dashboard.index', compact('last', 'respondants', 'still_required', 'responses'));

    }
    protected function fullscreen() {
        Carbon::setlocale('en');

        $required = [];
        $still_required = [];
        $abilities = [];
        $respondants = [];
        $last = Event::with(['type', 'responses'])->orderBy('created_at', 'desc')->first();
        $ids = [];
        $i = 0;
        $timeout = Carbon::now()->diffInMinutes(Carbon::parse($last->created_at)->copy()->addMinutes(45));
        if($timeout > 0)
        {
            return view('dashboard.no-event');
        }
        if($last){
            $responses = $last->responses()->get();
            foreach($responses as $res)
            {
                if(!in_array($res->member_id, $ids))
                {
                    $ids[] = $res->member_id;
                    $respondants[] = Member::find($res->member_id);
                }
            }
        }else{
            return view('dashboard.no-event');
        }
        foreach($respondants as $resp)
        {
             foreach($resp->qualification as $qual)
             {
                $abilities[] = $qual->shortcode;
             }
        }
        foreach($last->type as $type)
        {
            foreach($type->qualifications as $quals)
            {
                $still_required[] = $quals->shortcode;
            }
        }
        foreach($abilities as $ability)
        {
            $still_required = array_values($still_required);
            if(in_array($ability, $still_required)){
                for($x = 0; $x<sizeof($still_required); $x++){
                    if($still_required[$x] == $ability){
                        unset($still_required[$x]);
                    }
                }
            }
        }
      Log::info('finished');
    return view('fullscreen', compact('last', 'respondants', 'still_required', 'responses'));
    }
}
