<?php

namespace RFS_Manager\Http\Requests;

use RFS_Manager\Http\Requests\Request;
use Auth;

class QualificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shortcode' => ['required','min:2','unique:qualifications,shortcode,null,shortcode'],
            'qualification' => 'required'
        ];
    }
}
