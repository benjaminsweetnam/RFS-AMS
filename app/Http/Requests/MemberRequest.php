<?php

namespace RFS_Manager\Http\Requests;

use RFS_Manager\Http\Requests\Request;

use Auth;

class MemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'first' => 'required|min:2',
           'last' => 'required|min:2',
           'mobile' => ['required','regex:^(\+614)+([0-9]{8})$^','unique:members,mobile,'.$this->id]
        ];
    }
}
