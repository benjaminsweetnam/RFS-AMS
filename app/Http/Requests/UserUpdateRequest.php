<?php

namespace RFS_Manager\Http\Requests;

use Auth;
use RFS_Manager\Http\Requests\Request;
use RFS_Manager\User;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => ['required', 'email', 'unique:users,email,'.$this->id,'regex:^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$^'],
            'password' => 'sometimes|min:8|confirmed'];

    }
}

