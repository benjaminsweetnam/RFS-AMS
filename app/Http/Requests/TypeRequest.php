<?php

namespace RFS_Manager\Http\Requests;

use Illuminate\Support\Facades\Auth;
use RFS_Manager\Http\Requests\Request;

class TypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'callType' => 'required|min:3'
        ];
    }
}
