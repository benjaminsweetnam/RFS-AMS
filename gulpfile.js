var gulp = require('gulp'),
	shell = require('gulp-shell'),
	elixir = require('laravel-elixir'),
    livereload = require('laravel-elixir-livereload');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function(mix) {
    mix.sass(['app.scss'], 'public/css/app.css')
        .sass(['login.scss'], 'public/css/login.css')
        .sass(['dashboard.scss'], 'public/css/dashboard.css')
        .sass(['firecall.sass'], 'public/css/firecall.css')
        .browserify('app.js')
  	    .copy('resources/assets/images/RFS_Logo.svg', 'public/img')
        .version(['css/app.css', 'css/login.css', 'css/dashboard.css', 'css/firecall.css', 'js/app.js'])
        .browserSync({
    	port: 8000,
    	proxy: "192.168.10.10",
    	injectChanges: true,
    });
    mix.livereload();
});