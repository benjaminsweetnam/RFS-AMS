@extends('dashboard')

@section('page-header')
Register
@endsection

@section('content')
<ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
</ul>

{!! Form::open(array('url' => 'auth/register', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="input-group">
        {!! Form::label('name', 'Name')!!}
        {!! Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'name'))!!}
    </div>
    <div class="input-group">
        {!! Form::label('email', 'Email Address')!!}
        {!! Form::text('email', null, array('class' => 'form-control', 'placeholder'=>'email'))!!}
    </div>

    <div class="input-group">
        {!! Form::label('password', 'Password')!!}
        {!! Form::password('password', array('class' => 'form-control', 'placeholder'=>'password'))!!}
    </div>

    <div class="input-group">
        {!! Form::label('password_confirmation', 'Password Confirmation')!!}
        {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder'=>'password_confirmation'))!!}
    </div>

    <div class="btn-group">
        {!! Form::label('checkbox', 'Rember Me')!!}
        {!! Form::checkbox('checkbox', null, array('class' => 'form-control'))!!}
    </div>
    
    <div class="input-group">
        {!! Form::submit('Login', array('class'=>'btn btn-primary'))!!}
    </div>
{!! Form::close() !!}
@endsection