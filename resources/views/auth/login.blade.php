<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>RFS response app</title>
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>
    <div class="login">
        <div class="logo">
            <img src="{{ asset('img/RFS_Logo.svg') }}" alt="">
        </div>
        <p class="blurb">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
        <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
</ul>

{!! Form::open(array('url' => 'auth/login', 'method' => 'POST', 'class' => 'form')) !!}
    <div class="input-group">
        <span class="fa fa-envelope-o"></span>
        {!! Form::text('email', null, array('placeholder'=>'Email Address'))!!}
    </div>

    <div class="input-group">
        <span class="fa fa-lock"></span>
        {!! Form::password('password', array('placeholder'=>'Password'))!!}
    </div>

    <button type="submit">Next</button>
{!! Form::close() !!}
        <!-- <form action="">
            <div class="input-group">
                <span class="fa fa-envelope-o"></span>
                <input type="email" placeholder="Email Address">
            </div>
            <div class="input-group">
                <span class="fa fa-lock"></span>
                <input type="password" placeholder="Password">
            </div>
            <button type="submit">Next</button>
        </form> -->
    </div>
</body>
</html>



