<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>RFS response callout app | @yield('title')</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Optional theme -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="{{ elixir('css/firecall.css') }}">
</head>
<body>
	@include('partials.topbar')
	@section('sidebar')
	<div class="sidebar">
		<ul>
			<li><a href="{{route('home')}}" class="active"><i class="fa fa-user"></i>Dashboard</a></li>
			<li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
			<li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
			<li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
			<li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
		</ul>
	</div>
	@show
	<div class="dashboard" id="page">
		@if (session('status'))
			@include ('partials.alerts', session('status'))
        @endif
		@yield('content')
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/1.9.6/jquery.pjax.min.js"></script>
	<script>
    	$('div.alert').delay(2000).slideUp(300);
	</script>
	<script>
	    $('document').pjax('a', '#page');
	</script>
</body>
</html>