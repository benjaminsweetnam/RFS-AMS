            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </button>
            <div class="navbar">
                <div class="logo">
                    <object height="80" type="image/svg+xml" data="{{ asset('img/RFS_Logo.svg') }}"></object>
                    <span id="logo">NSW RURAL FIRE SERVICE</span>
                </div>
                <ul>
                @if(Auth::check())
                    <li role="presentation"><a class="new-firecall" href="{{url('/calls/new/')}}">New Firecall</a></li>
                    <li role="presentation"><a  href="{{route('users.edit', [Auth::user()->id])}}"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
                    <li role="presentation"><a  href="{{url('auth/logout')}}"><i class="fa fa-lock"></i> logout</a></li>
                @endif
                </ul>
                @if(Auth::guest())
                <a class="login" href="{{url('auth/login')}}"><i class="fa fa-user"></i></a>
                @endif
            </div>

            <div class="sidebar">
                    @if(Auth::check())
                    <nav>
                        <a class="new-firecall" href="{{url('/calls/new/')}}"><i class="fa fa-fire"></i> New Firecall</a>
                        <a  href="{{route('users.edit', [Auth::user()->id])}}"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>
                        <a  href="{{url('auth/logout')}}"><i class="fa fa-lock"></i> logout</a>
                        <a href="{{route('home')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        <a href="{{route('calls')}}"><i class="fa fa-fw  fa-fire-extinguisher"></i> Fire Calls</a>
                        <a href="{{route('users.index')}}"><i class="fa fa-fw fa-university"></i> User Accounts</a>
                        <a href="{{route('team.index')}}"><i class="fa fa-fw fa-users"></i> Members</a>
                    </nav>
                @endif()
            </div>