<div class="topbar">
		<span class="logo">
			<img src="{{ asset('img/RFS_Logo.svg') }}" alt="">
			<h2>NSW RURAL FIRE SERVICE</h2>
		</span>
		<span class="left-topbar">
			<a href="{{url('calls/new')}}" class="fire-call-btn">New Fire Call</a>
			<a href="{{url('auth/logout')}}" class="logout-btn">Logout</a>
			<span class="current-user">
				<p>{{ auth()->user()->name }}</p>
			</span>
		</span>
		<div class="clear"></div>
	</div>