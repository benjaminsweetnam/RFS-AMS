@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}" class="active"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
@include('partials.form-alerts')
  <div class="panel">
            <div class="title">
                <span>{{$member->first}} {{$member->last}}'s Qualifications</span><a id="add_account_button" href="{{route('members.index')}}">Back</a>
            </div>
            <table>
                <tr><th>Qualification Badge</th><th>ShortCode</th><th>Qualification</th><th>Added</th></tr>
                @foreach($memberQualifications as $quals)
                <tr><td><img class="badge" src="{{ asset('img/badges/'.$quals->shortcode.'.png') }}" alt=""></td><td>{{ $quals->shortcode }}</td><td>{{$quals->qualification}}</td><td>{{$quals->created_at}}</td><td><a href="{{ route('qualifications.unnassign', array($member, $quals->shortcode))}}">remove</a></td></tr>
                @endforeach
            </table>
            {!! Form::open() !!}
                {!! Form::select('qualifications', $qualifications, array(['class'=>'form-dropbox'])) !!}
                <button type="submit">Add Qualification</button>
            {!! Form::close() !!}
        </div>
@endsection