@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}" class="active"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
  <div class="panel">
            <div class="title">
                <span>Qualifications</span><a id="add_account_button" href="{{route('qualifications.create')}}">Add Qualification</a>
            </div>
            <table>
                @if(isset($qualifications))
                <tr><th>shortcode</th><th>Qualification</th><th>#Members</th><th>&nbsp;</th><th>&nbsp;</th></tr>
                @foreach($qualifications as $details)
                    <tr><td>{{$details->shortcode}}</td><td>{{$details->qualification}}</td><td>@if(count($details->members)>0){{count($details->members)}}@else<span>No Members</span>@endif<td><a href="{{route('qualifications.edit', $details->id)}}">Edit</a></td><td>{!! Form::open(array('route' => array('qualifications.destroy', $details->id), 'method' => 'delete')) !!}<button type="submit" >Remove Qualification</button>{!! Form::close() !!}</td></tr>
                @endforeach
                @endif
            </table>
            {!! $qualifications->render() !!}

        </div>
@endsection