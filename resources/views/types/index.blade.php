@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}" class="active"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
  <div class="panel">
            <div class="title">
                <span>Fire Call Types</span><a id="add_account_button" href="{{route('types.create')}}">Add Call Type</a>
            </div>
            <table>
                @if(isset($types))
                <tr><th>Call Type</th><th colspan="2">Required Qualifications</th><th>Ability To Respond</th><th>&nbsp;</th></tr>
                    @foreach($types as $type)
                        <tr><td>{{$type->callType}}</td>
                            <td>
                            @if(count($type->qualifications) > 0)
                                @foreach($type->qualifications as $qualification)
                                    <img class="badge" src="{{ asset('img/badges/'.$qualification->shortcode.'.png') }}">
                                @endforeach
                            @endif
                            </td>
                            <td><a href="{{ route('types.assign', $type->id)}}">Edit Requirements</a></td>
                            <td>something</td>
                            <td><a href="{{route('types.edit', $type->id)}}">Edit</a></td>
                            <td>{!! Form::open(array('route'=>array('types.destroy', $type->id), 'method'=>'delete')) !!}<button type="submit">Remove</button>{!! Form::close() !!}</td></tr>
                    @endforeach
                @else
                <tr><th>No Call Types Found</th></tr>
                @endif
            </table>
        </div>
@endsection