@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}" class="active"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
  <div class="panel">
            <div class="title">
                <span>Fire Call Types</span><a id="add_account_button" href="{{route('types.create')}}">Add Call Type</a>
            </div>
            <table>
                <tr><th>Call Type</th><th>Required Qualifications</th><th>Ability To Respond</th><th>&nbsp;</th></tr>
                    @foreach($types as $type)
                    <tr><td>{{$type->name}}</td><td></td><td><td>edit</td><td>remove</td></tr>
                    @endforeach
                <tr><th>No Call Types Found</th></tr>
                @endif
            </table>

        </div>
@endsection