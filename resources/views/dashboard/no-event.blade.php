@extends('dashboard')

@section('refresher')
<!-- <meta http-equiv="refresh" content="60"> -->
@endsection

@section('page-header')
Dashboard
@endsection

@section('content')
    <div class="panel">
        <div class="title">
            <p>No Current Firecalls</p>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
    $(document).ready(function(){

        var socket = io.connect('http://104.131.40.13:3000');

        socket.on('event-channel:RFS_Manager\\Events\\EventCreated', function(data) {
             console.log("Event recieved: %s", data);
             var url = $(this).attr("href");
             $.ajax({
                 dataType: "html",
                 data: JSON.stringify(data),
                 url: url,
                 cache: false,
                 success: function(html) {
                     console.log(html);
                     $("#page").html($(html).filter('#page').html());
                 }
             });
             socket.close();
         });

        });
    </script>
@endsection
