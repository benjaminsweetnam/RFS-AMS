@extends('dashboard')

@section('refresher')
@endsection

@section('page-header')
Dashboard
@endsection

@section('content')
    @if(count($last)>0)
        <div class="panel">
            <div class="title">
        <p>{{ Carbon\Carbon::parse($last->created_at)->format('D d M h:i A') }} {{ $last->message }}</p>
            </div>
        <div class="required">
                @if(!empty($still_required))
                    <h5>Skills Still Required</h5>
                    <ul>
                        @foreach($still_required as $req)
                        <li>
                            <img class="badge" src="{{ asset('img/badges/'.$req.'.png') }}" alt="Qualification Bade">
                        </li>
                        @endforeach
                    </ul>
                @else
                    <h5>All Skills Met</h5>
                @endif
            </div>
            <table>
                @if(!empty($respondants))
                    <tr><th>Name</th><th>Qualifications</th><th>ETA</th><th>&nbsp;</th></tr>
                    @foreach($respondants as $resp)
                    <tr>
                        <td>
                            {{$resp->last}}, {{$resp->first}}
                        </td>
                        <td>
                            @foreach($resp->qualification as $qualification)
                            <img class="badge" src="{{ asset('img/badges/'.$qualification->shortcode.'.png')}}" alt="{{$qualification->qualification}}">
                            @endforeach
                        </td>
                        <td class="time">
                            @if($resp->responses->last()->message == "no")
                                {{ $resp->responses->last()->message }}
                            @else
                                <span class="counter">{{ Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::parse($resp->responses->last()->created_at)->copy()->addMinutes($resp->responses->last()->message), False)  }}</span> mins
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @else
                <tr><th>Name</th><th>Qualifications</th><th>ETA</th><th>&nbsp;</th></tr>
                <tr><td></td><td><h2>No Responses!</h2></td></tr>
                @endif
            </table>
    </div>
    @endif
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
$(document).ready(function(){
    var socket = io.connect('http://104.131.40.13:3000');

    socket.on('connect', function() {
        console.log('socket connected');

            socket.on('event-channel:RFS_Manager\\Events\\EventCreated', function(data) {
                    console.log("Event recieved: %s", data);
                    var url = $(this).attr("href");
                    $.ajax({
                        dataType: "html",
                        data: JSON.stringify(data),
                        url: url,
                        cache: false,
                        success: function(html) {
                                console.log(html);
                                $("#page").html($(html).filter('#page').html());
                        }
                    });
            socker.close();
            });
        socket.on('response-channel:RFS_Manager\\Events\\ResponseRecieved', function(data) {
            console.log("Response recieved: %s", data);
            var url = $(this).attr("href");
            $.ajax({
                dataType: 'html',
                data: JSON.stringify(data),
                url: url,
                cache: false,
                success: function(html) {
                    console.log(html);
                    $("#page").html($(html).filter('#page').html());
                }
            });
            socket.close();
        });
    });
});
</script>
<script>
    function setZeros() {
    var placeholder = document.getElementsByClassName("counter");
        var container = document.getElementsByClassName("time");
        var array_size = placeholder.length;
        var value = 0;
        for(var i=array_size;i>0;i--)
        {
            value = placeholder[i-1].innerHTML;
            value = parseInt(value);
            if(value < 1)
            {
                container[i-1].innerHTML = "ARRIVED";
            }
        }
}
document.ready(setZeros());
</script>
<script>

function decrementor() {
    var placeholder = document.getElementsByClassName("counter");
    var container = document.getElementsByClassName("time");
    var array_size = placeholder.length;
    var value = 0;
    for(var i=array_size;i>0;i--)
    {
        value = placeholder[i-1].innerHTML;
        value = parseInt(value);
        if (value > 1){
         value -= 1;
         placeholder[i-1].innerHTML = value;
        } else {
         container[i-1].innerHTML = "arrived";
        }
    }
}
window.setInterval(function() {
    decrementor();
}, 60000);
</script>
@endsection