@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}" class="active"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
@include('partials.form-alerts')
  <div class="panel">
            {!! Form::open(array('url'=>'sendmessage')) !!}
                {!! Form::label('New Alert Message Here!') !!}
                {!! Form::text('message') !!}
                {!! Form::submit('Send Message') !!}
            {!! Form::close() !!}
        </div>
@endsection