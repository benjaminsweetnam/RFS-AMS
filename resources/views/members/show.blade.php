@extends('dashboard')

@section('title')
Team Member {{$member->first}} {{$member->last}}
@endsection

@section('page-header')
Team Members
@endsection

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}" class="active"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
  <div class="center">
    <h3>{{ $member->first }} {{ $member->last }}</h3>
    <p>Mobile: {{ $member->mobile }}</p>
    <a class="btn btn-primary" href="{{ route('members.edit', $member->id)}}">Edit</a>{!! Form::open(array('route' => array('members.destroy', $member->id), 'method' => 'delete')) !!}<button class="btn btn-danger" type="submit" >Delete Account</button>{!! Form::close() !!}
  </div>
@endsection