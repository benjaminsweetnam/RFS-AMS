@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}" class="active"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
<div class="panel">
            <div class="title">
                <span>Members</span><a id="add_account_button" href="{{route('members.create')}}">Add Member</a>
            </div>
            <table>
                @if(count($members) > 0)
                <tr><th>Name</th><th>Mobile</th><th>Qualifications</th><th>&nbsp;</th></tr>
                    @foreach($members as $member)
                        <tr>
                            <td>{{ $member->first }} {{ $member->last}}</td>
                            <td>{{ str_replace('+61', '0', trim($member->mobile)) }}</td>
                            <td>@if(count($member->qualification) > 0) @foreach($member->qualification as $qualification)<img class="badge" src="{{ asset('img/badges/'.$qualification->shortcode.'.png') }}" alt="qualification badge">@endforeach @endif</td>
                            <td><a href="{{ route('qualifications.assign', $member->id) }}">Edit Qualifications</a></td>
                            <td><a href="{{route('members.edit', $member->id)}}">Edit</a></td>
                            <td>{!! Form::open(array('route' => array('members.destroy', $member->id), 'method' => 'delete')) !!}<button type="submit" >Delete Account</button>{!! Form::close() !!}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
@endsection
