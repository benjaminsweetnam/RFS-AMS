@extends('dashboard')

@section('page-header')
Fire Calls
@endsection

@section('breadcrumbs')
<ol class="breadcrumb">
    <li>
        <i class="fa fa-dashboard"></i>  <a href="{{ route('home') }}">Dashboard</a>
    </li>
    <li>
        <i class="fa fa-fire-extinguisher"></i> <a href="{{ route('calls') }}"> Fire Calls</a>
    </li>
    <li>
        <i class="fa fa-mobile-phone"> Responses</i>
    </li>
</ol>
@endsection

@section('content')
<?php $number = 0; $price = 0; ?>

<?php use Carbon\Carbon; ?>


@foreach($messages as $message)
<?php 
	$number += 1;
	$price += $message->price; 
?>

@endforeach
<p>{{ $number }} messages at a cost of ${{ $price }}</p> 
<hr>
@foreach($messages as $message)
@if($message->direction == "inbound")

<?php list($response, $response_time) = explode(' ' , $message->body)?>

<p>Message Id: {{$message->sid}}</p>
<p>

<?php
    $sent = $message->date_sent;
    $response = (int)$response_time;
    $due = Carbon::parse($sent)->addMinutes($response);
    $current = Carbon::now();
    $diff = $current->diffForHumans($due);
?>

 
{{$diff}} ETA </p>
<p>Direction: {{$message->direction}}</p>
<p>Date Sent: {{$message->date_sent}}</p>
<p>Delivery Status: {{$message->status}}</p>
<hr>
<br>
@endif
@endforeach
@endsection