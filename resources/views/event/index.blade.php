@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}" class="active"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
        <div class="panel">
            <div class="title">
                <p>Fire Calls</p>
            </div>
                @if(count($events) > 0)
                    <table>
                        <tr>
                            <th>Time</th>
                            <th>Message</th>
                            <th>Responses</th>
                            <th>Call Type</th>
                            <th>&nbsp;</th>
                        </tr>
                @foreach($events as $event)
                    <tr>
                        <td class="event-data">{{ Carbon\Carbon::parse($event->created_at)->toDayDateTimeString() }}</td>
                        <td class="event-data">{{ $event->message }}</td>
                        <td>{{ $event->responses->count() }}</td>
                        <td>
                        @foreach($event->type as $type)
                            {{ $type->callType }}
                        @endforeach
                        </td>
                    </tr>
                @endforeach
                    </table>
                    <div class="center-container">
                        <div class="center">
                            {!! $events->render() !!}
                         </div>
                    </div>
                @else
                    <h2 class="empty-table">No Fire Calls Made</h2>
                @endif

    </table>

</div>
@endsection