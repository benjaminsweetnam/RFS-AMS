@extends('dashboard')


@section('content')
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
'location', 'message', 'FireDangerRating', 'TimeToIncident'
{!! Form::Model($event, array('action' => 'MemberController@update')) !!}
	@include('event.form')
{!! Form::close() !!}
@endsection