@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}" class="active"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
<div class="panel">
            <div class="title">
                <p>New Event</p>
            </div>
            {!! Form::Model($event, array('action' => 'EventController@store',  'class' => 'form-horizontal')) !!}
            	@include('event.form', ['buttonText' => 'Update Event'])
            {!! Form::close() !!}
</div>
<script>
    $('form.form-horizontal button[type=submit]').click(function(e){
        e.preventDefault();

        var form = jQuery.this.parents("form:first");
        var dataString = form.serialize();
        var formAction = form.attr('action');

    $.ajax({
        type: "POST",
        url : formAction,
        data: dataString,
        success : function(data){
            alert("testing ajax forms: " + data);
        }

    }, "json");
    });
</script>
@endsection