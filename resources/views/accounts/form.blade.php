    {!! Form::hidden('id', $user->id) !!}
    <div class="input-group">
        {!! Form::label('name', 'Name', array('class'=>'control-label'))!!}
        <div class="col-sm-6">
            {!! Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'name'))!!}
        </div>
    </div>
    <div class="input-group">
        {!! Form::label('email', 'Email Address', array('class'=>'control-label'))!!}
        <div class="col-sm-6">
        {!! Form::text('email', null, array('class' => 'form-control', 'placeholder'=>'email'))!!}
        </div>
    </div>

    <div class="input-group">
        {!! Form::label('password', 'Password', array('class'=>'control-label'))!!}
        <div class="col-sm-6">
        {!! Form::password('password', array('class' => 'form-control', 'placeholder'=>'password'))!!}
        </div>
    </div>

    <div class="input-group">
        {!! Form::label('password_confirmation', 'Password Confirmation', array('class'=>'control-label'))!!}
        <div class="col-sm-6">
        {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder'=>'password_confirmation'))!!}
        </div>
    </div>

    <div class="input-group">
        <button type="submit">{{$buttonText}}</button>
    </div>

