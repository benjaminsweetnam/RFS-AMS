@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}" class="active"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
<div class="panel">
            <div class="title">
                <span>User Accounts</span><a id="add_account_button" href="{{route('users.create')}}">Add Account</a>
            </div>
                <table>
                    <tr><th>Name</th><th>Email</th><th></th><th>&nbsp;</th></tr>
                    @foreach($accounts as $account)
                    <tr><td>{{$account->name}}</td><td>{{$account->email}}</td><td><a href="{{route('users.edit', $account->id)}}">Edit</a></td><td><a>Delete</a></td></tr>
                    @endforeach
                </table>
            </div>

<!-- <div class="row center">
    <table class="center">
    @foreach($accounts as $account)
        <tr><td><b>name:</b></td><td>{{$account->name}}</td><td><b>username:</b></td><td>{{$account->email}}</td><td><a class="btn btn-default" href="{{route('users.edit', $account->id)}}">Edit</a></td><td>{!! Form::open(array('route' => array('users.destroy', $account->id), 'method' => 'delete')) !!}<button class="btn btn-default" type="submit">Remove</button>{!! Form::close() !!}</td></tr>
    @endforeach
    </table>
</div> -->
@endsection
