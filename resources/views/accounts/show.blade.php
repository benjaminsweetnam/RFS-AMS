@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}" class="active"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
		<p class="user-accounts"><span><b>name:</b> {{$account->name}}</span>&nbsp;&nbsp;<span><b>username:</b> {{$account->email}}</span>&nbsp;&nbsp;<a class="btn btn-primary" href="{{route('users.edit', $account->id)}}">Edit</a>&nbsp;&nbsp;<a class="btn btn-danger" href="{{route('users.destroy', $account->id)}}">Delete</a></p>
@endsection