@extends('dashboard')

@section('sidebar')
    <div class="sidebar">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-user"></i>Dashboard</a></li>
            <li><a href="{{route('events')}}"><i class="fa fa-file-text"></i>Fire Calls</a></li>
            <li><a href="{{route('users.index')}}" class="active"><i class="fa fa-users"></i>User Accounts</a></li>
            <li><a href="{{route('members.index')}}"><i class="fa fa-lightbulb-o"></i>Members</a></li>
            <li><a href="{{route('qualifications.index')}}"><i class="fa fa-graduation-cap"></i>Qualifications</a></li>
            <li><a href="{{route('types.index')}}"><i class="fa fa-fire"></i>Fire Call Types</a></li>
        </ul>
    </div>
@endsection

@section('content')
@include('partials.form-alerts')
<div class="panel">
            <div class="title">
                <p>New Account</p>
            </div>

    {!! Form::model($user, array('action' => array('AccessController@store', $user))) !!}
        @include('accounts.form', ['buttonText' => 'Create Account'])
    {!! Form::close() !!}
</div>
@endsection