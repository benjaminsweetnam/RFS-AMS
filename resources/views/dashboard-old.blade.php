<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>RFS response callout app | @yield('title')</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<!-- Optional theme -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('refresher')
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>
    @include ('partials.nav')
    <div class="page" id="page">
        @section('page-header')
            replace me
        @show
        @yield('header-right')
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @yield('content')
    </div>
    <!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.pjax/1.9.6/jquery.pjax.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script src="{{ elixir('js/app.js') }}"></script>
<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
        var socket = io('http://192.168.10.10');
        socket.on('connect', function(){
            console.log("connected client");
        });
        socket.on('message', function(data) {
            console.log("data recieved: ", data);
            location.reload();
        });
        socket.on('disconnect', function(){
                console.log('disconnected from client');
            });
    </script>
<script>
    $('div.alert').delay(2000).slideUp(300);
</script>
<script>
    $('document').pjax('a', '#page');
</script>
@if ( Config::get('app.debug') )
  <script type="text/javascript">
    document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
  </script>
@endif
</body>
</html>
